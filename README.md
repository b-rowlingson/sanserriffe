San Serrife
===========

San Serriffe was first described in a special feature in The Guardian
newspaper on April 1st 1977. A scan from that was georeferenced via
the outline map and then the main map scaled and located. Features
were digitised from that into several layers.

The geopackage contains some vector layers and a QGIS project. The
project links to some raster files which I am not currently
sharing.

For amusement only.

Barry Rowlingson <b.rowlingson@gmail.com>




![Map of Upper Caisse](uppercaisse.jpg "Upper Caisse")

![Map of Lower Caisse](lowercaisse.jpg "Lower Caisse")

